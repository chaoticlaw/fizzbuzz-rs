/*
 * FIZZBUZZ
 *
 * Solution by Minh Nguyen
 */

fn main() {
    // We need to define the starting number, and the last number to iterate through

    /* Rust expects constants to have types defined
     * i32 represents 32 bits of space,
     * to handle both positive and negative numbers
     */
    const NUM_START:	i32 =   1;
    const NUM_END:		i32 = 100;

    // Then define which numbers will trigger fizz and buzz
 	const TRIGGER_ONE:	i32 =   3;
 	const TRIGGER_TWO:	i32 =   5;

 	// Now to create the loop
	for counter in NUM_START..(NUM_END + 1) { // Upper bounds mean it'll stop before 100 so '+ 1' is required
		if (counter % TRIGGER_ONE == 0) & (counter % TRIGGER_TWO == 0) {
			println!("Fizzbuzz");
		} else if counter % TRIGGER_ONE == 0 {
			println!("Fizz");
		} else if counter % TRIGGER_TWO == 0 {
			println!("Buzz");
		}
		else {
			println!("{}", counter);
		}
	}
}
